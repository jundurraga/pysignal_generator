from fractions import Fraction
from . tools import *
import numpy as np
from scipy import signal
from scipy.interpolate import interp1d


def generate_modulated_noise(fs: float = 44100.0,
                             duration: float = 1.0,  # duration in seconds
                             n_repetitions: int = 1, # sampling frequency
                             amplitude: float = 1.0,  # between -1 and 1
                             f_noise_low: float = 300.0,  # phase in rad
                             f_noise_high: float = 700.0,  # phase in rad
                             attenuation:float = 0.0,  # in dB
                             modulation_frequency:float = 0.0,  # frequency in Hz
                             modulation_phase:float = 0.0,  # frequency in Hz
                             modulation_index:float = 0.0,  # frequency in Hz
                             only_return_parameters: bool = False,  # dummy var
                             round_to_cycle:bool = True,  # round frequencies to fit cycles in time
                             cycle_rate: float = 44100,  # this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
                             alternate_repetitions: bool = False,  # if true, the polarity of the repetition will be alternated
                             rise_fall_time: float = 0.0,
                             fit_signal_spectrum: bool = False,
                             round_next_power_2=False,
                             reference_rms= None):

    value = np.array([])
    time = np.array([])
    if only_return_parameters:
        #  this is used to return only the structure
        return value, time
    if round_next_power_2:
        duration = next_power_2(round(fs*duration)) / fs
    if round_to_cycle:
        f_fs = Fraction(fs)
        f_cr = Fraction(cycle_rate)
        n1, d1 = (f_fs.numerator, f_fs.denominator)
        n2, d2 = (f_cr.numerator, f_cr.denominator)
        clcm = lcm(n1 * d2, d1 * n2) / (d1 * d2)
        m1 = clcm / fs
        _min_duration = m1 / cycle_rate
        duration = np.ceil(duration / _min_duration) * _min_duration
        # fit modulation rate  
        modulation_frequency = round(duration * modulation_frequency) / duration

    time = np.expand_dims(np.arange(0, fs * duration) / fs, axis=1)
    n = time.size
    if np.mod(n, 2) == 0:
        n_uniq_freq = int((n / 2 + 1))
    else:
        n_uniq_freq = int((n + 1) / 2)

    freq = np.arange(0, n_uniq_freq) * fs / n
    p = attenuation / (20 * np.log10(0.5))
    amp_noise = np.zeros((n_uniq_freq, 1))

    # defining spectral magnitude
    for _i in np.arange(1, n_uniq_freq):
        f = freq[_i]
        if (f >= f_noise_low) and (f <= f_noise_high):
            amp_noise[_i] = 1 / (f ** p)
        else:
            amp_noise[_i] = 0

    # Phase generation
    phase_noise = 2 * np.pi * np.random.rand(n_uniq_freq, 1)
    spectrum_noise = amp_noise * np.exp(1j * phase_noise)

    # synthesized noise
    noise = np.fft.irfft(spectrum_noise, n, axis=0)
    # modulated noise
    mod_amp = (1 - modulation_index * np.cos(2 * np.pi * modulation_frequency * time + modulation_phase)) / (1 + modulation_index)
    _amplitude = amplitude * mod_amp
    if fit_signal_spectrum:
        noise = fit_spectrum(noise, mod_amp, amp_noise)
    noise = noise / np.max(np.abs(noise))
    noise = _amplitude * noise
    if reference_rms:
        noise = reference_rms / rms(noise) * noise  # normalize noise to have required rms
    value = np.tile(noise, n_repetitions)
    return value, time


def generate_noise_alt_delay_signal(kwargs):
    fs = kwargs.setdefault('fs', 44100.0)  # sampling frequency
    duration = kwargs.setdefault('duration', 1.0)  # duration in seconds
    n_repetitions = kwargs.setdefault('n_repetitions', 1)  # sampling frequency
    amplitude = kwargs.setdefault('amplitude', 1.0)  # between -1 and 1
    f_noise_low = kwargs.setdefault('f_noise_low', 300.0)  # phase in rad
    f_noise_high = kwargs.setdefault('f_noise_high', 700.0)  # phase in rad
    attenuation = kwargs.setdefault('attenuation', 0.0)  # in dB
    itd = kwargs.setdefault('itd', 0.0)
    alt_itd = kwargs.setdefault('alt_itd', 0.0)
    diotic_control = kwargs.setdefault('diotic_control', False)  #
    itd_mod_rate = kwargs.setdefault('itd_mod_rate', 2.0)  #
    modulation_frequency = kwargs.setdefault('modulation_frequency', 0.0)  # frequency in Hz
    modulation_phase = kwargs.setdefault('modulation_phase', 0.0)  # frequency in Hz
    modulation_index = kwargs.setdefault('modulation_index', 0.0)  # frequency in Hz
    rise_fall_time = kwargs.setdefault('rise_fall_time', 0.0)
    description = kwargs.setdefault('description', '')
    filter_type = kwargs.setdefault('filter_type', 'gaussian')
    include_triggers = kwargs.setdefault('include_triggers', False)
    pulse_width = kwargs.setdefault('pulse_width', 0.0005)
    trigger_amp = kwargs.setdefault('trigger_amp', 1.0)
    fit_signal_spectrum = kwargs.setdefault('fit_signal_spectrum', False)
    only_return_parameters = kwargs.setdefault('only_return_parameters', False)  # dummy var
    round_to_cycle = kwargs.setdefault('round_to_cycle', True)  # round frequencies to fit cycles in time
    cycle_rate = kwargs.setdefault('cycle_rate', fs)  # this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
    alternate_repetitions = kwargs.setdefault('alternate_repetitions', False)  # if true, the polarity of the repetition will be alternated
    ref_spl = kwargs.setdefault('ref_spl', None)

    value = np.array([])
    time = np.array([])
    kwargs.pop('only_return_parameters')
    if only_return_parameters:
        #  this is used to return only the structure
        return value, time

    if round_to_cycle:
        f_fs = Fraction(fs)
        f_cr = Fraction(cycle_rate)
        n1, d1 = (f_fs.numerator, f_fs.denominator)
        n2, d2 = (f_cr.numerator, f_cr.denominator)
        clcm = lcm(n1 * d2, d1 * n2) / (d1 * d2)
        m1 = clcm / fs
        _min_duration = m1 / cycle_rate
        itd_mod_rate = 1 / (round((1 / itd_mod_rate) / _min_duration) * _min_duration)
        duration = np.ceil(duration / _min_duration) * _min_duration
        # fit modulation rate
        modulation_frequency = round(duration * modulation_frequency) / duration

    block_duration = 1 / itd_mod_rate
    # we ensure even number of events for epoch continuity
    duration = np.ceil(duration* itd_mod_rate / 2.0) * 2.0 / itd_mod_rate

    modulation_frequency = round(modulation_frequency / itd_mod_rate) * itd_mod_rate

    time = np.expand_dims(np.arange(0, fs * duration) / fs, axis=1)

    l_signal = np.empty((0, 1))
    r_signal = np.empty((0, 1))
    n_cycles = int(round(duration * itd_mod_rate))

    for i in range(n_cycles):
        # synthesized noise (we make  it longer than original block to apply a delay safely without edge distorsions
        if np.mod(i, 2) == 0:
            delay = alt_itd
        else:
            delay = itd

        l_signal_block, _ = generate_modulated_noise({'duration': block_duration + abs(2 * delay),
                                                     'f_noise_low': f_noise_low,
                                                     'f_noise_high': f_noise_high,
                                                     'fs': fs})
        r_signal_block = delay_signal(l_signal_block, fs, delay)
        vini_pos = np.floor(l_signal_block.shape[0] / 2 - block_duration * fs / 2)
        samp_vec = np.arange(vini_pos, vini_pos + round(block_duration * fs)).astype(np.int)
        l_signal = np.concatenate((l_signal, l_signal_block[samp_vec, :]))
        r_signal = np.concatenate((r_signal, r_signal_block[samp_vec, :]))

    m_amp = (1 - modulation_index * np.cos(2 * np.pi * modulation_frequency * time + modulation_phase))
    l_signal = l_signal * m_amp
    r_signal = r_signal * m_amp
    if diotic_control:
        l_signal = r_signal

    target_spectrum = create_spectrum({'n_samples': l_signal.shape[0],
                                       'attenuation': attenuation,
                                       'fs': fs,
                                       'f_low': f_noise_low,
                                       'f_high': f_noise_high,
                                       'type': filter_type})
    # fit resulting signals to improve desired bandwidth
    # generates rise fall window cWindow = ones(size(lSignal))
    if rise_fall_time > 0:
        _window = rise_fall_window('fs', fs,
                                   'duration', duration,
                                   'rise_fall_time', rise_fall_time)
        l_signal = _window * l_signal
        r_signal = _window * r_signal
        if fit_signal_spectrum:
            l_signal = fit_spectrum(l_signal, m_amp * _window, target_spectrum)
            r_signal = fit_spectrum(r_signal, m_amp * _window, target_spectrum)
        else:
            l_signal = fft_filter(l_signal, target_spectrum) * _window
            r_signal = fft_filter(r_signal, target_spectrum) * _window

    # equalize energy
    rms_ref = rms(l_signal)
    rSignal = rms_ref / rms(r_signal) * r_signal
    n_samples = l_signal.shape[0]
    _signal = np.hstack((l_signal, r_signal))
    # normalize
    _signal = _signal / np.max(np.abs(_signal))
    # compute normalize rms for calibration
    _rms = rms(l_signal) / np.max(np.abs(l_signal))
    if ref_spl is not None:
        scale_factor = 10 ** (amplitude / 20) * ref_spl / _rms
    else:
        scale_factor = amplitude

    _signal = scale_factor * _signal

    if include_triggers:
        # value = np.zeros(n * n_repetitions, 3)
        # triggers = clicks('Duration', s.Duration, 'Rate', 1 / s.Duration, ...
        # 'PulseWidth', s.PulseWidth, 'Amplitude', s.TriggerAmp, ...
        # 'RoundToCycle', 0, 'Fs', s.Fs)
        # cChannelData =[triggers, cSignal]
        a = 1
    else:
        value = np.zeros((n_samples * n_repetitions, 2))
        _channel_data = _signal

    for i in np.arange(n_repetitions):
        factor = 1.0
        if alternate_repetitions:
            factor = (-1) ^ (i + 1)

        value[i * n_samples: (i + 1) * n_samples, :] = _channel_data
        if include_triggers:
            value[i * n_samples: (i + 1) * n_samples, 2::] = factor * _channel_data[:, 1::]
        else:
            value[i * n_samples: (i + 1) * n_samples, :] = factor * _channel_data
    return value, time


def generate_shaped_noise(fs: float = 44100.0,
                          duration: float = 1.0,  # duration in seconds
                          n_repetitions: int = 1, # sampling frequency
                          amplitude: float = 1.0,  # between -1 and 1
                          target_data: np.array = np.array([]),  # target file to extract spectrum
                          only_return_parameters: bool = False,  # dummy var
                          fit_signal_spectrum: bool = False,
                          round_next_power_2=False,
                          reference_rms= None):

    value = np.array([])
    time = np.array([])
    if only_return_parameters:
        #  this is used to return only the structure
        return value, time
    if round_next_power_2:
        duration = next_power_2(round(fs*duration)) / fs

    n = int(fs * duration)
    time = np.expand_dims(np.arange(0, n) / fs, axis=1)

    # defining spectral magnitude
    data = np.mean(target_data, axis=1)
    f, pxx = signal.welch(data, fs, 'flattop', data.shape[0] / 25.0, scaling='spectrum')
    _int_fun = interp1d(f, pxx, kind='linear', fill_value="extrapolate")
    new_freq = np.arange(0, n // 2 + 1) * fs / n
    pxx_int = np.atleast_2d(_int_fun(new_freq)).T
    pxx_int[pxx_int < 0] = 0
    # freq = np.arange(0, data.shape[0] // 2 + 1) * fs / data.shape[0]
    # _fft_magnitude = np.atleast_2d(np.abs(np.fft.rfft(data, axis=0))).T * 2 / data.shape[0]

    # Phase generation
    phase_noise = 2 * np.pi * np.random.rand(pxx_int.shape[0], 1)
    spectrum_noise = pxx_int ** 0.5 * np.exp(1j * phase_noise)

    # synthesized noise

    noise = np.fft.irfft(spectrum_noise, axis=0)
    # modulated noise
    if fit_signal_spectrum:
        noise = fit_spectrum(noise, np.ones(noise.shape), pxx_int)
    noise = noise / np.max(np.abs(noise))
    noise = amplitude * noise
    if reference_rms:
        noise = reference_rms / rms(noise) * noise  # normalize noise to have required rms
    value = np.tile(noise, n_repetitions)
    return value, time
