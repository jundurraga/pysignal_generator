import numpy as np
from fractions import Fraction, gcd


def lcm(num1, num2):
    return num1 * num2 / gcd(num1, num2)


def sym_alt_phase(kwargs):
    fs = kwargs.setdefault('fs', 44100.0)  #sampling frequency
    duration = kwargs.setdefault('duration', 1.0)  #duration in seconds
    n_repetitions = kwargs.setdefault('n_repetitions', 1)  #sampling frequency
    amplitude = kwargs.setdefault('amplitude', 1.0)  #between -1 and 1
    carrier_phase = kwargs.setdefault('carrier_phase', 0.0)  #phase in rad
    carrier_alt_phase = kwargs.setdefault('carrier_alt_phase', np.pi / 2.0) # phase in rad
    carrier_n_alt_phase_per_mod_cycle = kwargs.setdefault('carrier_n_alt_phase_per_mod_cycle', 1) # phase in rad
    carrier_rate = kwargs.setdefault('carrier_rate', 500.0)  #frequency in Hz
    pulse_width = kwargs.setdefault('pulse_width', 0.0001)  # frequency in sec
    modulation_frequency = kwargs.setdefault('modulation_frequency', 0.0)  #frequency in Hz
    modulation_phase = kwargs.setdefault('modulation_phase', 0.0)  #frequency in Hz
    modulation_index = kwargs.setdefault('modulation_index', 0.0)  #frequency in Hz
    only_return_parameters = kwargs.setdefault('only_return_parameters', False)  #dummy var
    round_to_cycle = kwargs.setdefault('round_to_cycle', True)  #round frequencies to fit cycles in time
    cycle_rate = kwargs.setdefault('cycle_rate', fs)  #this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
    alternate_repetitions = kwargs.setdefault('alternate_repetitions', False)  #if true, the polarity of the repetition will be alternated
    rise_fall_time = kwargs.setdefault('rise_fall_time', 0.0)
    description = kwargs.setdefault('description', '')
    value = np.array([])
    time = np.array([])
    kwargs.pop('only_return_parameters')
    if only_return_parameters:
        #  this is used to return only the structure
        return value, time

    f = Fraction(carrier_n_alt_phase_per_mod_cycle)  #this is to fit to infinit fractions
    n_alt, d_alt = (f.numerator, f.denominator)
    if round_to_cycle:
        #  compute least common multiple between fs and cycle_rate
        f_fs = Fraction(fs)
        f_cr = Fraction(cycle_rate)
        n1, d1 = (f_fs.numerator, f_fs.denominator)
        n2, d2 = (f_cr.numerator, f_cr.denominator)
        clcm = lcm(n1*n2, d1*d2)/(d1*d2)
        m1 = clcm / fs
        _min_duration = m1 / cycle_rate
        #  now we change the input duration to the closet minduration multiple
        duration = np.ceil(duration / _min_duration) * _min_duration
        kwargs['duration'] = duration
        #  fit carrier freq
        _carrier_rate = round(duration * carrier_rate) / duration
        kwargs['carrier_rate'] = _carrier_rate
        #  fit modulation rate
        modulation_frequency = round(duration * modulation_frequency) / duration
        # refit modulation so that the number of phase oscillations fits in the full period time
        modulation_frequency = round(duration * modulation_frequency* n_alt / (2 * d_alt)) / (duration* n_alt / (2 * d_alt))
        kwargs['modulation_frequency'] = modulation_frequency

    time = np.expand_dims(np.arange(0, fs * duration-1) / fs, axis=1)
    _time_pulses = np.expand_dims(np.arange(0, _carrier_rate * duration-1) / _carrier_rate, axis=1)
    _temp_phase = np.zeros(_time_pulses.shape)
    phase_alt_period = 2.0 / (modulation_frequency * n_alt / d_alt)
    phase_alt_rate = 1.0 / phase_alt_period
    kwargs['alt_phase_rate'] = phase_alt_rate

    #  fit _alt_phase to move together with envelope phase
    n_cycles_phase = np.round(duration * phase_alt_rate)
    _alt_phase = 0
    for i in np.arange(n_cycles_phase):
        _single_phase = (carrier_phase - carrier_alt_phase) * \
                        (unitary_step(_time_pulses - (1.0 / phase_alt_rate) * (_alt_phase) /
                                      (2.0 * np.pi) - i / phase_alt_rate) -
                         unitary_step(_time_pulses - (1.0 /phase_alt_rate) * _alt_phase /
                                      (2.0 * np.pi) - phase_alt_period / 2.0 - i / phase_alt_rate))
        _temp_phase += _single_phase
    _temp_phase += carrier_alt_phase
    _temp_phase *= 1.0 / (2.0 * np.pi * carrier_rate)
    _time_pulses += _temp_phase
    _amplitude = amplitude * (1.0 - modulation_index * np.cos(2.0 * np.pi *
                                                              modulation_frequency * time + modulation_phase))

    t_pulse = np.expand_dims(np.arange(0, fs * 2*pulse_width - 1) / fs, axis=1)
    _signal_template = unitary_step(t_pulse) - 2 * unitary_step(t_pulse - pulse_width) + unitary_step( t_pulse - 2* pulse_width)
    _signal = np.zeros(time.shape)
    idx_pulse = np.round(_time_pulses * fs).astype(np.int)
    idx_pulse = np.squeeze(idx_pulse[np.where(idx_pulse >= 0)[0]])
    for i in idx_pulse:
        _ini = i
        _end = np.minimum(_signal.size, i + _signal_template.size).astype(np.int)
        _w = _end - _ini
        _signal[i:_end] = _signal_template[0:_w] * _amplitude[_ini, 0]

    # normalize sinusoidal to prevent amplitudes higher than 1 when modulating
    _signal = amplitude * _signal / np.max(np.abs(_signal))
    n_samples = _signal.shape[0]

    value = np.zeros((n_samples * n_repetitions, 1))
    for i in np.arange(n_repetitions):
        factor = 1.0
        if alternate_repetitions:
            factor = (-1.0) ** i
        value[i * n_samples: (i + 1) * n_samples, :] = factor * _signal
    return value, time


def unitary_step(x):
    return (x >= 0.0).astype(np.float)


