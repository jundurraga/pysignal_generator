import sweep_functions as sf
import numpy as np
import matplotlib.pyplot as plt
import sounddevice as sd
fs = 100e3
sweep, time = sf.constant_envelope_sweep(fs=fs, f_1=0, f_2=3000.0, duration=1.0,
                                         rise_fall_time_ini=0.1,
                                         rise_fall_time_end=0.01,
                                         frequency_threshold=1,
                                         attenuation=-0)
plt.figure()
plt.plot(time, sweep)
plt.show()
n = sweep.shape[0]
freq_ori = np.arange(0, n // 2 + 1) * fs / n
plt.plot(freq_ori, 20*np.log10(np.abs(np.fft.rfft(sweep, axis=0))))
plt.show()
print(sd.query_devices())
sd.default.device = 9
sd.default.samplerate = fs

sd.play(np.hstack((sweep, sweep)), blocking=True, blocksize=4096)
