import pysignal_generator.noise_functions as nf
import soundfile as sf
import numpy as np
import matplotlib.pyplot as plt
import os
from os import sep


_path = os.path.abspath(os.path.dirname(__file__))
_file_path = os.path.join(_path + sep + '..' + sep + '..' + sep + 'digits' + sep + '7.wav')


data, fs = sf.read(_file_path)
data = np.atleast_2d(data).T
shaped_noise, _ = nf.generate_shaped_noise(fs=fs, target_data=data)
n = data.shape[0]
freq_ori = np.arange(0, n // 2 + 1) * fs / n
plt.plot(freq_ori, np.abs(np.fft.rfft(data, axis=0)))
n_n = shaped_noise.shape[0]
new_freq = np.arange(0, n_n // 2 + 1) * fs / n_n
plt.plot(new_freq, np.abs(np.fft.rfft(shaped_noise, axis=0)))
plt.show()

