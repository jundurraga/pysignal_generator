import numpy as np
from fractions import Fraction
from tools import rise_fall_window, lcm


def sin_alt_phase(kwargs):
    fs = kwargs.setdefault('fs', 44100.0)  #sampling frequency
    duration = kwargs.setdefault('duration', 1.0)  #duration in seconds
    n_repetitions = kwargs.setdefault('n_repetitions', 1)  #sampling frequency
    amplitude = kwargs.setdefault('amplitude', 1.0)  #between -1 and 1
    carrier_phase = kwargs.setdefault('carrier_phase', 0.0)  #phase in rad
    carrier_alt_phase = kwargs.setdefault('carrier_alt_phase', np.pi / 2.0) # phase in rad
    carrier_n_alt_phase_per_mod_cycle = kwargs.setdefault('carrier_n_alt_phase_per_mod_cycle', 1) # phase in rad
    carrier_frequency = kwargs.setdefault('carrier_frequency', 500.0)  #frequency in Hz
    modulation_frequency = kwargs.setdefault('modulation_frequency', 0.0)  #frequency in Hz
    modulation_phase = kwargs.setdefault('modulation_phase', 0.0)  #frequency in Hz
    modulation_index = kwargs.setdefault('modulation_index', 0.0)  #frequency in Hz
    only_return_parameters = kwargs.setdefault('only_return_parameters', False)  #dummy var
    round_to_cycle = kwargs.setdefault('round_to_cycle', True)  #round frequencies to fit cycles in time
    cycle_rate = kwargs.setdefault('cycle_rate', fs)  #this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
    alternate_repetitions = kwargs.setdefault('alternate_repetitions', False)  #if true, the polarity of the repetition will be alternated
    rise_fall_time = kwargs.setdefault('rise_fall_time', 0.0)
    description = kwargs.setdefault('description', '')
    value = np.array([])
    time = np.array([])
    kwargs.pop('only_return_parameters')
    if only_return_parameters:
        #  this is used to return only the structure
        return value, time

    f = Fraction(carrier_n_alt_phase_per_mod_cycle)  #this is to fit to infinit fractions
    n_alt, d_alt = (f.numerator, f.denominator)
    if round_to_cycle:
        #  compute least common multiple between fs and cycle_rate
        f_fs = Fraction(fs)
        f_cr = Fraction(cycle_rate)
        n1, d1 = (f_fs.numerator, f_fs.denominator)
        n2, d2 = (f_cr.numerator, f_cr.denominator)
        clcm = lcm(n1*d2, d1*n2)/(d1*d2)
        m1 = clcm / fs
        _min_duration = m1 / cycle_rate
        #  now we change the input duration to the closet minduration multiple
        duration = np.ceil(duration / _min_duration) * _min_duration
        kwargs['duration'] = duration
        #  fit carrier freq
        _carrier_frequency  = round(duration * carrier_frequency) / duration
        kwargs['carrier_frequency'] = _carrier_frequency
        #  fit modulation rate
        modulation_frequency = round(duration * modulation_frequency) / duration
        # refit modulation so that the number of phase oscillations fits in the full period time
        modulation_frequency = round(duration * modulation_frequency* n_alt / (2 * d_alt)) / (duration* n_alt / (2 * d_alt))
        kwargs['modulation_frequency'] = modulation_frequency

    time = np.expand_dims(np.arange(0, fs * duration-1) / fs, axis=1)
    _temp_phase = np.zeros(time.shape)
    phase_alt_period = 2.0 / (modulation_frequency * n_alt / d_alt)
    phase_alt_rate = 1.0 / phase_alt_period
    kwargs['alt_phase_rate'] = phase_alt_rate
    n_cycles = np.round(duration * phase_alt_rate)
    #  fit _alt_phase to move together with envelope phase
    _alt_phase = 0
    for i in np.arange(0, n_cycles):
        _single_phase = (carrier_phase - carrier_alt_phase) * \
                        (unitary_step(time - (1.0 / phase_alt_rate) * (_alt_phase) /
                                      (2.0 * np.pi) - i / phase_alt_rate) -
                         unitary_step(time - (1.0 /phase_alt_rate) * _alt_phase /
                                      (2.0 * np.pi) - phase_alt_period / 2.0 - i / phase_alt_rate))
        _temp_phase += _single_phase
    _temp_phase += carrier_alt_phase
    _amplitude = (1.0 - modulation_index * np.cos(2.0 * np.pi * modulation_frequency * time + modulation_phase)) / (1.0 + modulation_index)
    _carrier = np.sin(2.0 * np.pi * _carrier_frequency * time + _temp_phase)
    _signal = _amplitude * _carrier
    #  normalize sinusoidal to prevent amplitudes higher than 1 when modulating
    _signal = amplitude * _signal / np.max(np.abs(_signal))
    n_samples = _signal.shape[0]

    #  generates rise fall window
    if rise_fall_time > 0:
        _window = rise_fall_window({'fs': fs, 'Duration': duration, 'rise_fall_time': rise_fall_time})
        _signal = _window * _signal
    value = np.zeros((n_samples * n_repetitions, 1))
    for i in np.arange(n_repetitions):
        factor = 1.0
        if alternate_repetitions:
            factor = (-1.0) ** i
        value[i * n_samples: (i + 1) * n_samples, :] = factor * _signal
    return value, time


def unitary_step(x):
    return (x >= 0.0).astype(np.float)


def sinusoidal(kwargs):
    fs = kwargs.setdefault('fs', 44100.0)  #sampling frequency
    duration = kwargs.setdefault('duration', 1.0)  #duration in seconds
    n_repetitions = kwargs.setdefault('n_repetitions', 1)  #sampling frequency
    amplitude = kwargs.setdefault('amplitude', 1.0)  #between -1 and 1
    carrier_phase = kwargs.setdefault('carrier_phase', 0.0)  #phase in rad
    carrier_frequency = kwargs.setdefault('carrier_frequency', 500.0)  #frequency in Hz
    modulation_frequency = kwargs.setdefault('modulation_frequency', 0.0)  #frequency in Hz
    modulation_phase = kwargs.setdefault('modulation_phase', 0.0)  #frequency in Hz
    modulation_index = kwargs.setdefault('modulation_index', 0.0)  #frequency in Hz
    only_return_parameters = kwargs.setdefault('only_return_parameters', False)  #dummy var
    round_to_cycle = kwargs.setdefault('round_to_cycle', True)  #round frequencies to fit cycles in time
    cycle_rate = kwargs.setdefault('cycle_rate', fs)  #this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
    alternate_repetitions = kwargs.setdefault('alternate_repetitions', False)  #if true, the polarity of the repetition will be alternated
    rise_fall_time = kwargs.setdefault('rise_fall_time', 0.0)

    description = kwargs.setdefault('description', '')
    value = np.array([])
    time = np.array([])
    kwargs.pop('only_return_parameters')
    if only_return_parameters:
        #  this is used to return only the structure
        return value, time
    _carrier_frequency = carrier_frequency
    if round_to_cycle:
        #  compute least common multiple between fs and cycle_rate
        f_fs = Fraction(fs)
        f_cr = Fraction(cycle_rate)
        n1, d1 = (f_fs.numerator, f_fs.denominator)
        n2, d2 = (f_cr.numerator, f_cr.denominator)
        clcm = lcm(n1*d2, d1*n2)/(d1*d2)
        m1 = clcm / fs
        _min_duration = m1 / cycle_rate
        #  now we change the input duration to the closet minduration multiple
        duration = np.ceil(duration / _min_duration) * _min_duration
    kwargs['duration'] = duration
    #  fit carrier freq
    _carrier_frequency = round(duration * carrier_frequency) / duration
    kwargs['carrier_frequency'] = _carrier_frequency
    #  fit modulation rate
    modulation_frequency = round(duration * modulation_frequency) / duration
    # refit modulation so that the number of phase oscillations fits in the full period time
    kwargs['modulation_frequency'] = modulation_frequency

    time = np.expand_dims(np.arange(0, fs * duration - 1) / fs, axis=1)
    #  fit _alt_phase to move together with envelope phase
    _amplitude = (1.0 - modulation_index * np.cos(2.0 * np.pi * modulation_frequency * time + modulation_phase)) / (1.0 + modulation_index)
    _carrier = np.sin(2.0 * np.pi * _carrier_frequency * time + carrier_phase)
    _signal = _amplitude * _carrier

    ##  normalize sinusoidal to prevent amplitudes higher than 1 when modulating
    _signal = amplitude * _signal / np.max(np.abs(_signal))
    n_samples = _signal.shape[0]

    ## generates rise fall window
    if rise_fall_time > 0:
        _window = rise_fall_window({'fs': fs, 'duration': duration, 'rise_fall_time': rise_fall_time})
        _signal = _window * _signal
    value = np.zeros((n_samples * n_repetitions, 1))
    for i in np.arange(n_repetitions):
        factor = 1.0
        if alternate_repetitions:
            factor = (-1.0) ** i
        value[i * n_samples: (i + 1) * n_samples, :] = factor * _signal
    return value, time


def sin_alt_frequency(kwargs):
    fs = kwargs.setdefault('fs', 44100.0)  #sampling frequency
    duration = kwargs.setdefault('duration', 1.0)  #duration in seconds
    n_repetitions = kwargs.setdefault('n_repetitions', 1)  #sampling frequency
    alt_rate = kwargs.setdefault('alt_rate', 6.0)  # sampling frequency
    amplitude_1 = kwargs.setdefault('amplitude_1', 1.0)  #between -1 and 1
    carrier_phase_1 = kwargs.setdefault('carrier_phase_1', 0.0)  #phase in rad
    carrier_frequency_1 = kwargs.setdefault('carrier_frequency_1', 500.0)  #frequency in Hz
    modulation_frequency_1 = kwargs.setdefault('modulation_frequency_1', 0.0)  #frequency in Hz
    modulation_phase_1 = kwargs.setdefault('modulation_phase_1', 0.0)  #frequency in Hz
    modulation_index_1 = kwargs.setdefault('modulation_index_1', 0.0)  # frequency in Hz

    amplitude_2 = kwargs.setdefault('amplitude_2', 1.0)  # between -1 and 1
    carrier_frequency_2 = kwargs.setdefault('carrier_frequency_2', 500.0)  # frequency in Hz
    carrier_phase_2 = kwargs.setdefault('carrier_phase_2', 0.0)  # phase in rad
    modulation_frequency_2 = kwargs.setdefault('modulation_frequency_2', 0.0)  # frequency in Hz
    modulation_phase_2 = kwargs.setdefault('modulation_phase_2', 0.0)  # frequency in Hz
    modulation_index_2 = kwargs.setdefault('modulation_index_2', 0.0)  # frequency in Hz
    only_return_parameters = kwargs.setdefault('only_return_parameters', False)  #dummy var
    round_to_cycle = kwargs.setdefault('round_to_cycle', True)  #round frequencies to fit cycles in time
    cycle_rate = kwargs.setdefault('cycle_rate', fs)  #this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
    alternate_repetitions = kwargs.setdefault('alternate_repetitions', False)  #if true, the polarity of the repetition will be alternated
    rise_fall_time = kwargs.setdefault('rise_fall_time', 0.0)
    ref_spl = kwargs.setdefault('ref_spl', 0.0)  # this parameter is necessary to calibrate stimuli SPL
    description = kwargs.setdefault('description', '')
    value = np.array([])
    time = np.array([])
    kwargs.pop('only_return_parameters')
    if only_return_parameters:
        #  this is used to return only the structure
        return value, time

    if round_to_cycle:
        #  compute least common multiple between fs and cycle_rate
        f_fs = Fraction(fs)
        f_cr = Fraction(cycle_rate)
        n1, d1 = (f_fs.numerator, f_fs.denominator)
        n2, d2 = (f_cr.numerator, f_cr.denominator)
        clcm = lcm(n1*d2, d1*n2)/(d1*d2)
        m1 = clcm / fs
        _min_duration = m1 / cycle_rate
        alt_rate = 1 / (np.ceil((1.0 / alt_rate) / _min_duration) * _min_duration)

    # here we make sure this total duration meets all requirements
    duration = np.ceil(duration * alt_rate) / alt_rate
    carrier_frequency_1 = round(carrier_frequency_1 / alt_rate) * alt_rate
    kwargs['carrier_frequency_1'] = carrier_frequency_1
    modulation_frequency_1 = round(modulation_frequency_1 / alt_rate) * alt_rate
    kwargs['modulation_frequency_1'] = modulation_frequency_1

    duration = np.ceil(duration * alt_rate) / alt_rate
    carrier_frequency_2 = round(carrier_frequency_2 / alt_rate) * alt_rate
    kwargs['carrier_frequency_2'] = carrier_frequency_2
    modulation_frequency_2 = round(modulation_frequency_2 / alt_rate) * alt_rate
    kwargs['modulation_frequency_2'] = modulation_frequency_2

    time = np.expand_dims(np.arange(0, fs * duration) / fs, axis=1)

    n_samples = time.size
    n_cycles = np.round(duration * alt_rate).astype(np.int)
    rep_period = 1 / alt_rate
    switch_n_samples = np.round(rep_period * fs).astype(np.int)
    time_switch = np.arange(0, switch_n_samples) / fs
    _signal = np.zeros(time.shape)
    _signal_1 = np.sin(2 * np.pi * carrier_frequency_1 * time_switch + carrier_phase_1)
    _signal_1 = _signal_1 * (1 - modulation_index_1 * np.cos(2 * np.pi * modulation_frequency_1 * time_switch + 
                                                             modulation_phase_1))
    _signal_2 = np.sin(2 * np.pi * carrier_frequency_2 * time_switch + carrier_phase_2)
    _signal_2 = _signal_2 * (1 - modulation_index_2 * np.cos(2 * np.pi * modulation_frequency_2 * time_switch +
        modulation_phase_2))

    # normalize
    _signal_1 = _signal_1 / max(abs(_signal_1));
    _signal_2 = _signal_2 / max(abs(_signal_2));
    # compute normalized rms for calibration
    _rms_1 = np.sqrt(np.mean(_signal_1**2))
    _rms_2 = np.sqrt(np.mean(_signal_2**2))
    if ref_spl:
        scale_factor_1 = 10 ** (amplitude_1 / 20) * ref_spl / _rms_1
        scale_factor_2 = 10 ** (amplitude_2 / 20) * ref_spl / _rms_2
    else:
        scale_factor_1 = amplitude_1
        scale_factor_2 = amplitude_2

    _signal_1 = scale_factor_1 * _signal_1
    _signal_2 = scale_factor_2 * _signal_2
    kwargs['rms'] = np.sqrt(np.mean(_signal_1 ** 2))

    _window = np.ones((switch_n_samples, 1))
    if rise_fall_time > 0:
        _window = rise_fall_window({'fs': fs, 'Duration': rep_period, 'rise_fall_time': rise_fall_time})

    for i in range(n_cycles):
        # progressbar(i / ncycles);
        ini_pos = np.round(rep_period * fs).astype(np.int) * i
        end_pos = ini_pos + switch_n_samples
        if np.mod(i, 2) == 0:
            _signal[ini_pos: end_pos, 0] = _signal_1
        else:
            _signal[ini_pos: end_pos, 0] = _signal_2

        _signal[ini_pos: end_pos] = _window * _signal[ini_pos: end_pos]

    value = np.zeros((n_samples * n_repetitions, 1))
    for i in np.arange(n_repetitions):
        factor = 1.0
        if alternate_repetitions:
            factor = (-1.0) ** i
        value[i * n_samples: (i + 1) * n_samples, :] = factor * _signal
    return value, time


def sin_alt_ipd(kwargs):
    fs = kwargs.setdefault('fs', 44100.0)  # sampling frequency
    duration = kwargs.setdefault('duration', 1.0)  # duration in seconds
    n_repetitions = kwargs.setdefault('n_repetitions', 1)  # sampling frequency
    amplitude = kwargs.setdefault('amplitude', 1.0)  # between -1 and 1
    carrier_phase = kwargs.setdefault('carrier_phase', 0.0)  # phase in rad
    carrier_frequency = kwargs.setdefault('carrier_frequency', 500.0)  # frequency in Hz
    ipd = kwargs.setdefault('ipd', -np.pi / 2)
    alt_ipd = kwargs.setdefault('alt_ipd', np.pi / 2)
    ipm_rate = kwargs.setdefault('ipm_rate', 6.8)

    modulation_frequency = kwargs.setdefault('modulation_frequency', 41.0)  # frequency in Hz
    modulation_phase = kwargs.setdefault('modulation_phase', 0.0)  # frequency in Hz
    modulation_index = kwargs.setdefault('modulation_index', 1.0)  # frequency in Hz
    only_return_parameters = kwargs.setdefault('only_return_parameters', False)  # dummy var
    round_to_cycle = kwargs.setdefault('round_to_cycle', True)  # round frequencies to fit cycles in time
    cycle_rate = kwargs.setdefault('cycle_rate', fs)  # this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
    diotic_control = kwargs.setdefault('diotic_control', 0)
    alternate_repetitions = kwargs.setdefault('alternate_repetitions',
                                              False)  # if true, the polarity of the repetition will be alternated
    rise_fall_time = kwargs.setdefault('rise_fall_time', 0.0)
    description = kwargs.setdefault('description', '')
    ref_spl = kwargs.setdefault('ref_spl', None)
    include_triggers = kwargs.setdefault('include_triggers', 1) #  if true add trigger channel
    pulse_width_trigger = kwargs.setdefault('pulse_width_trigger', 0.0005) #  duration in seconds
    trigger_amp = kwargs.setdefault('trigger_amp', 1) # between -1 and 1
    # s = kwargs.setdefault('FNoiseLow', 0);% initial frequency masking noise[Hz]
    # s = kwargs.setdefault('FNoiseHigh', 1300);% end frequency masking noise[Hz]
    # s = kwargs.setdefault('NoiseAmp', 0);% amplitude between -1 and 1, or dB if using refSPL
    # s = kwargs.setdefault('DioticNoise', 0);% If true, same noise in both ears

    value = np.array([])
    time = np.array([])
    kwargs.pop('only_return_parameters')
    if only_return_parameters:
        #  this is used to return only the structure
        return value, time

    if round_to_cycle:
        #  compute least common multiple between fs and cycle_rate
        f_fs = Fraction(fs)
        f_cr = Fraction(cycle_rate)
        n1, d1 = (f_fs.numerator, f_fs.denominator)
        n2, d2 = (f_cr.numerator, f_cr.denominator)
        clcm = lcm(n1*d2, d1*n2)/(d1*d2)
        m1 = clcm / fs
        _min_duration = m1 / cycle_rate
        ipm_rate = 1 / (np.ceil((1.0 / ipm_rate) / _min_duration) * _min_duration)

    # here we make sure this total duration meets all requirements
    phase_alt_period = 2.0 / ipm_rate
    duration = np.ceil(duration / phase_alt_period) * phase_alt_period
    modulation_frequency = round(modulation_frequency / ipm_rate) * ipm_rate
    # fit carrier freq
    carrier_frequency = round(carrier_frequency/ipm_rate) * ipm_rate

    kwargs['carrier_frequency'] = carrier_frequency
    kwargs['ipm_rate'] = ipm_rate
    kwargs['modulation_frequency'] = modulation_frequency
    kwargs['phase_alt_period'] = phase_alt_period
    kwargs['duration'] = duration

    time = np.atleast_2d(np.arange(0, fs * duration - 1) / fs).T
    _temp_phase = np.zeros(time.shape)
    phase_alt_rate = 1 / phase_alt_period
    n_cycles = int(round(duration * phase_alt_rate))

    for i in range(n_cycles):
        _single_phase = (ipd / 2 - alt_ipd / 2)*(unitary_step(time - i / phase_alt_rate) -
                                                 unitary_step(time - phase_alt_period/2 - i / phase_alt_rate))
        _temp_phase = _temp_phase + _single_phase

    _temp_phase_1 = _temp_phase + alt_ipd / 2
    if diotic_control:
        _temp_phase_2 = _temp_phase_1
    else:
        _temp_phase_2 = -_temp_phase_1
    
    _amplitude = (1 - modulation_index * np.cos(2 * np.pi * modulation_frequency * time + modulation_phase)) / (1.0 + modulation_index)
    _carrier_1 = np.sin(2 * np.pi * carrier_frequency * time + _temp_phase_1 + carrier_phase)
    _carrier_2 = np.sin(2 * np.pi * carrier_frequency * time + _temp_phase_2 + carrier_phase)
    _signal = _amplitude * np.hstack((_carrier_1, _carrier_2))
    n_samples = _signal.shape[0]
    #  generates rise fall window
    _window = np.ones(_signal.shape)
    if rise_fall_time > 0:
        rise_fall_window({'fs': fs, 'Duration': duration, 'rise_fall_time': rise_fall_time})
    _signal *= _window 
    
    # normalize
    _signal = _signal / np.max(np.abs(_signal), axis=0)
    #  compute normalize rms for calibration
    _rms = np.std(_signal[:,0])
    if ref_spl is not None:
        scale_factor = 10 ** (amplitude / 20.) * ref_spl/_rms
    else:
        scale_factor = amplitude
    
    _signal = scale_factor * _signal
    rms = np.std(_signal[:, 0])

    # % once generated the signal, we add the noise
    # 
    # [noise_1, ~, ~] = generateNoise('FLow', s.FNoiseLow, ...
    #     'FHigh',s.FNoiseHigh, ...
    #     'Fs', s.Fs, ...
    #     'Duration', s.Duration, ...
    #     'RoundToCycle', 0 ...
    #     );
    # [noise_2, ~, ~] = generateNoise('FLow', s.FNoiseLow, ...
    #     'FHigh',s.FNoiseHigh, ...
    #     'Fs', s.Fs, ...
    #     'Duration', s.Duration, ...
    #     'RoundToCycle', 0 ...
    #     );
    # 
    # if ref_spl is not _none:
    #     n1_RMS = rms(noise_1);
    #     n2_RMS = rms(noise_2);
    #     n_f_1 = 10^(s.NoiseAmp/20) * ref_spl/n1_RMS;
    #     n_f_2 = 10^(s.NoiseAmp/20) * ref_spl/n2_RMS;
    # else
    #     n_f_1 = s.NoiseAmp;
    #     n_f_2 = s.NoiseAmp;
    # end
    # 
    # noise_1 = n_f_1 * noise_1;
    # if s.DioticNoise
    #     noise_2 = noise_1;
    # else
    #     noise_2 = n_f_2 * noise_2;
    # end
    # s.RMSNoise = rms(noise_1);
    # % add the noise
    # _signal = _signal + [noise_1, noise_2];

    if include_triggers:
        value = np.zeros((int(n_samples*n_repetitions), 3))
        triggers, _ = pulse_train(**{'duration': duration, 'rate': 1.0 / duration,
                                 'pulse_width': pulse_width_trigger, 'amplitude': trigger_amp,
                                 'round_to_cycle': 0, 'fs': fs})
        _channel_data = np.hstack((triggers, _signal))
    else:
        value = np.zeros((n_samples * n_repetitions, 2))
        _channel_data = _signal

    # compute normalize rms for calibration
    n_rms = scale_factor*np.std(_channel_data[:, -1]) / np.max(np.abs(_channel_data[:, -1]))

    for i in range(n_repetitions):
        factor = 1.0
        if alternate_repetitions:
            factor = (-1.0) ** i
        value[i * n_samples: (i + 1) * n_samples, :] = _channel_data
        if include_triggers:
            value[i * n_samples : (i + 1) * n_samples, 1:] = factor * _channel_data[:, 1:]
        else:
            value[i * n_samples : (i + 1) * n_samples, :] = factor * _channel_data

    return value, time


def unitary_step(x):
    return (x >= 0).astype(np.float)


def pulse_train(kwargs):
    fs = kwargs.setdefault('fs', 44100); # sampling frequency
    duration = kwargs.setdefault('duration', 1) #duration in seconds
    n_epetitions = kwargs.setdefault('n_epetitions', 1)# sampling frequency
    amplitude = kwargs.setdefault('amplitude', 1)# between -1 and 1
    phase = kwargs.setdefault('phase', 0)#  phase in rad
    rate = kwargs.setdefault('rate', 500)# frequency in Hz
    pulse_width = kwargs.setdefault('pulse_width', 0.00050)# pulse width in seconds
    alternating = kwargs.setdefault('alternating', False)# alternate pulses polarity

    only_return_parameters = kwargs.setdefault('only_return_parameters', False)  # dummy var
    round_to_cycle = kwargs.setdefault('round_to_cycle', True)  # round frequencies to fit cycles in time
    cycle_rate = kwargs.setdefault('cycle_rate', fs)  # this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
    description = kwargs.setdefault('description', '')

    value = np.array([])
    time = np.array([])

    if only_return_parameters:
        #  this is used to return only the structure
        return value, time

    if round_to_cycle:
        #  compute least common multiple between fs and cycle_rate
        f_fs = Fraction(fs)
        f_cr = Fraction(cycle_rate)
        n1, d1 = (f_fs.numerator, f_fs.denominator)
        n2, d2 = (f_cr.numerator, f_cr.denominator)
        clcm = lcm(n1 * d2, d1 * n2) / (d1 * d2)
        m1 = clcm / fs
        _min_duration = m1 / cycle_rate
        # now we change the input duration to the closet minduration multiple
        duration = np.ceil(duration / _min_duration) * _min_duration
        # fit rate to duration
        rate = np.round(duration *rate) / duration

    time = np.atleast_2d(np.arange(0, fs * duration - 1) / fs).T
    _signal = np.zeros(time.shape)
    _template = np.ones((int(np.round(fs * pulse_width)), 1))
    n_cycles = int(round(duration * rate))
    _phase = phase - np.floor(phase / (2 * np.pi))*  2 * np.pi
    for i in range(n_cycles):
        _pulse_pos = int(np.round((_phase / (2 * np.pi) + i) * fs / rate))
        _signal[_pulse_pos: _pulse_pos + _template.size] = _template

    _signal = amplitude * _signal
    n_samples = _signal.size
    value = np.zeros((n_samples * n_epetitions, 1))
    for i in range(n_epetitions):
        value[i * n_samples: (i + 1) * n_samples] = _signal * (-1.0) ** (i * alternating)
    return value, time
