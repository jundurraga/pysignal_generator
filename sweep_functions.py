import numpy as np
from fractions import Fraction
from tools import *
import matplotlib.pyplot as plt


def constant_envelope_sweep(**kwargs):
    fs = kwargs.setdefault('fs', 44100.0)  # sampling frequency
    duration = kwargs.setdefault('duration', 1.0)  # duration in seconds
    n_repetitions = kwargs.setdefault('n_repetitions', 1)  # sampling frequency
    amplitude = kwargs.setdefault('amplitude', 1.0)  # between -1 and 1
    attenuation = kwargs.setdefault('attenuation', 0.0)  # between 0 and  - inf  # in dB
    f_1 = kwargs.setdefault('f_1', 00)  # initial frequency
    f_2 = kwargs.setdefault('f_2', 00)  # end frequency
    residual_threshold = kwargs.setdefault('residual_threshold', -80)  # in dB
    frequency_threshold = kwargs.setdefault('frequency_threshold', 2)  # in dB

    only_return_parameters = kwargs.setdefault('only_return_parameters', False)  # dummy var
    round_to_cycle = kwargs.setdefault('round_to_cycle', True)  # round frequencies to fit cycles in time
    cycle_rate = kwargs.setdefault('cycle_rate', fs)  # this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
    alternate_repetitions = kwargs.setdefault('alternate_repetitions', False)  # if true, the polarity of the repetition will be alternated
    rise_fall_time_ini = kwargs.setdefault('rise_fall_time_ini', 0.0)
    rise_fall_time_end = kwargs.setdefault('rise_fall_time_end', 0.0)
    description = kwargs.setdefault('description', '')
    value = np.array([])
    time = np.array([])
    kwargs.pop('only_return_parameters')
    if only_return_parameters:
        #  this is used to return only the structure
        return value, time

    if round_to_cycle:
        #  compute least common multiple between fs and cycle_rate
        f_fs = Fraction(fs)
        f_cr = Fraction(cycle_rate)
        n1, d1 = (f_fs.numerator, f_fs.denominator)
        n2, d2 = (f_cr.numerator, f_cr.denominator)
        clcm = lcm(n1 * d2, d1 * n2) / (d1 * d2)
        m1 = clcm / fs
        _min_duration = m1 / cycle_rate
        #  now we change the input duration to the closet minduration multiple
        duration = np.ceil(duration / _min_duration) * _min_duration
        kwargs['duration'] = duration

    t_ini = 0.1
    n = int((duration + t_ini) * fs)
    t_dur = duration * 4
    time = np.arange(n) / fs
    n_samples = int(np.floor(t_dur * fs))
    n_uniq_freq = n_samples // 2 + 1
    freq = np.arange(0, n_uniq_freq) * fs / n_samples
    p = attenuation / (20 * np.log10(0.5))
    amp_noise = np.zeros((n_uniq_freq, 1))

    # defining spectral magnitude
    for _i in np.arange(1, n_uniq_freq):
        f = freq[_i]
        if (f >= f_1) and (f <= f_2):
            amp_noise[_i] = 1 / (f ** p)
        else:
            amp_noise[_i] = 0

    # Phase generation
    phase = np.zeros((n_uniq_freq, 1))
    group_delay = np.zeros((n_uniq_freq, 1))
    amp = np.zeros((n_uniq_freq, 1))

    n_f1 = int(np.argwhere(freq >= f_1)[0])
    n_f2 = int(np.argwhere(freq <= f_2)[-1])

    # defining spectral magnitude
    f_c1 = np.minimum(f_1 * 0.1, fs/2.0)
    s_21 = 20 * np.log10(np.exp(1)) * (f_1 - f_c1) ** 2 / (2 * 3)
    f_c2 = np.minimum(f_2 * 0.99, fs/2)
    s_22 = 20 * np.log10(np.exp(1)) * (f_2 - f_c2) ** 2 / (2 * 3)
    for ni in range(n_uniq_freq):
        f = freq[ni]
        if (f >= f_c1) and (f <= f_c2):
            amp[ni] = 1 / ((f + 1) ** p)
        elif f < f_c1:
            amp[ni] = 1 / ((freq[n_f1] + 1) ** p) * np.exp(-(f - f_c1) ** 2 / (2 * s_21))
        elif f > f_c2:
            amp[ni] = 1 / ((freq[n_f2] + 1) ** p) * np.exp(-(f - f_c2) ** 2 / (2 * s_22))

    # group delay generation in samples
    group_delay[n_f1] = t_ini
    # tg is in seconds
    c = duration / np.sum(np.abs(amp ** 2))
    for ni in range(n_uniq_freq):
        if ni > n_f1:
            group_delay[ni] = group_delay[ni - 1] + c * np.abs(amp[ni]) ** 2
    group_delay[0: n_f1] = group_delay[n_f1]
    group_delay[n_f2::] = group_delay[n_f2]

    # Phase Generation(-int(Tg))
    _sum = 0
    f_step = 2 * np.pi * fs / n_samples
    phase[0] = 0
    for ni in range(n_uniq_freq):
        _sum = _sum + f_step * group_delay[ni]
        phase[ni] = _sum
    # for ni in range(n_uniq_freq):
    #      phase[ni] = phase[ni] - phase[-1] * freq[ni]/(fs/2)

    # phase = phase * fs / (fs / 2)
    phase *= -1.0
    spectrum = amp * np.exp(1j * phase)
    _w = np.zeros((n_samples, 1))
    n_w_ini = 0
    n_w_end = n
    if rise_fall_time_ini > 0 or rise_fall_time_end > 0:
        _window = rise_fall_window_asymmetric({'fs': fs, 'duration': duration,
                                               'rise_fall_time_ini': rise_fall_time_ini,
                                               'rise_fall_time_end': rise_fall_time_end,
                                               })
        _w[n_w_ini: n_w_ini + _window.shape[0], :] = _window
        _w[0:n_w_ini] = _window[0]
        _w[n_w_end::] = _window[-1]

    # synthesized noise
    tol_freq = np.inf
    tol_resid = np.inf
    _v = np.arange(n_f1, n_f2).astype(np.int)
    while tol_resid > residual_threshold or tol_freq > frequency_threshold:
        spectrum = amp * np.exp(1j * phase)
        _signal = np.fft.irfft(spectrum, n_samples, axis=0)
        _signal *= _w
        _fft = np.fft.rfft(_signal, n_samples, axis=0)
        phase = np.angle(_fft)
        tol_freq = np.max(20 * np.log10(np.abs(_fft[_v]) / np.abs(amp[_v])))
        tol_resid = np.max(20. * np.log10(np.abs(_signal))[n_w_end::])
        print('frequency tolerance {:}, residual tolerance {:}'.format(tol_freq, tol_resid))
        plt.plot(freq, 20. * np.log10(amp))
        plt.plot(freq, 20. * np.log10(np.abs(_fft)))
        plt.axhline(residual_threshold)
        plt.ylim(-100, 6)

    _signal *= amplitude / np.abs(_signal).max()
    _signal = _signal[0:n_w_end]
    # _signal = fit_spectrum(_signal, _w, amp)
    #  generates rise fall window
    value = np.zeros((n * n_repetitions, 1))
    for i in np.arange(n_repetitions):
        factor = 1.0
        if alternate_repetitions:
            factor = (-1.0) ** i
        value[i * n: (i + 1) * n, :] = factor * _signal
    # time = np.arange(0, value.shape[0]) / fs
    return value, time

