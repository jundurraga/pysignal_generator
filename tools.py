import numpy as np
import math


def fit_spectrum(time_signal, time_shape_constrain, target_spectrum):
    n = time_signal.shape[0]

    if np.mod(n, 2) == 0:
        n_uniq_freq = int((n / 2 + 1))
    else:
        n_uniq_freq = int((n + 1) / 2)

    error = np.inf
    tol = 1e-10
    delta_error = np.inf
    _fft = np.fft.rfft(time_signal, axis=0)
    n_iter = 1
    fitted_signal = None
    while (delta_error > tol) and (n_iter < 1000):
        _phase = np.angle(_fft)
        # reconstruct full fft
        full_spectrum = target_spectrum * np.exp(1j * _phase)
        fitted_signal = np.fft.irfft(full_spectrum, n, axis=0) * time_shape_constrain
        _fft = np.fft.rfft(fitted_signal, axis=0)
        current_error = np.std(np.abs(_fft[range(n_uniq_freq)] - target_spectrum))
        delta_error = abs(error - current_error)
        error = current_error
        n_iter = n_iter + 1
        print('error: {:.6f}'.format(error))

    print('n_iter: {:}'.format(n_iter))
    return fitted_signal


def create_spectrum(kwargs):
    n_samples = kwargs.setdefault('n_samples', 0)
    attenuation = kwargs.setdefault('attenuation', 0)
    fs = kwargs.setdefault('fs', 44100)
    f_low = kwargs.setdefault('f_low', 0)
    f_high = kwargs.setdefault('f_high', fs / 2.0)
    type = kwargs.setdefault('type', 'gaussian')

    if np.mod(n_samples, 2) == 0:  # if even
        n_uniq_freq = int(np.ceil(1 + n_samples / 2))
    else:  # if odd
        n_uniq_freq = int(np.ceil((n_samples + 1) / 2))

    amp = np.zeros((n_uniq_freq, 1))
    if f_low == 0.0 and f_high == fs / 2.0:
        amp = np.ones((n_uniq_freq, 1))

    freq = np.arange(n_uniq_freq) * fs / n_uniq_freq

    if type == 'gaussian':
        fc = f_low + (f_high - f_low) / 2.0
        s2 = 20*np.log10(np.exp(1)) * (f_low - fc) ** 2.0 / (2.0 * 3.0)
        for _i in range(n_uniq_freq):
            f = freq[_i]
            amp[_i] = np.exp(-(f - fc) ** 2.0 / (2.0 * s2))

    if type == 'rectangular':
        p = attenuation / (20 * np.log10(0.5))
        # defining spectral magnitude
        for _i in np.arange(n_uniq_freq):
            f = freq[_i]
            if (f >= f_low) and (f <= f_high):
                amp[_i] = 1 / ((f+1)**p)
            else:
                amp[_i] = 0
    return amp


def fft_filter(time_signal, target_spectrum):
    _fft = np.fft.rfft(time_signal, axis=0)
    assert _fft.shape[0] == target_spectrum.shape[0]
    _phase = np.angle(_fft)
    # reconstruct full fft
    fitted_signal = np.fft.irfft(np.abs(_fft) * target_spectrum * np.exp(_phase),  axis=0)
    return fitted_signal


def filt_filt_fft_filter(time_signal, target_spectrum):
    signal = fft_filter(time_signal, target_spectrum)
    signal = fft_filter(np.flip(signal, axis=0), target_spectrum)
    signal = np.flip(signal, axis=0)
    return signal


def delay_signal(signal, fs, delay):
    n_samples = signal.shape[0]
    yf = np.fft.rfft(signal, axis=0)
    f_samples = yf.shape[0]
    m = delay * fs
    vector = np.expand_dims(np.arange(f_samples), axis=1)
    phase_shift = np.exp(-1j * 2 * np.pi * vector * m / n_samples)
    d_signal = np.fft.irfft(yf * phase_shift, axis=0, n=n_samples)
    return d_signal


def rise_fall_window(kwargs):
    fs = kwargs.get('fs', 44100.0)
    duration = kwargs.get('duration', 0.0)
    rise_fall_time = kwargs.get('rise_fall_time', 0.0)
    type = kwargs.get('type', 'linear')
    out_window = np.squeeze(np.ones((int(round(fs * duration) - 1), 1)))
    ##  generates rise fall window
    if rise_fall_time > 0.0:
        _n_win = int(round(fs * rise_fall_time))
        rise_fall_samples = np.arange(_n_win)
        if type == 'linear':
            out_window[0:_n_win] = np.arange(0, _n_win) / _n_win
            out_window[-_n_win:] = np.arange(_n_win, 0., -1) / _n_win
        else:
            out_window[0:_n_win] = np.sin(2.0 * np.pi * rise_fall_samples / (4.0 *(_n_win - 1))) ** 2.0
            out_window[-_n_win:] = np.cos(2.0 * np.pi * rise_fall_samples / (4.0 * (_n_win - 1))) ** 2.0
    return np.atleast_2d(out_window).T


def rise_fall_window_asymmetric(kwargs):
    fs = kwargs.get('fs', 44100.0)
    duration = kwargs.get('duration', 0.0)
    rise_fall_time_ini = kwargs.get('rise_fall_time_ini', 0.0)
    rise_fall_time_end = kwargs.get('rise_fall_time_end', 0.0)
    type = kwargs.get('type', 'linear')
    out_window = np.squeeze(np.ones((int(round(fs * duration) - 1), 1)))
    ##  generates rise fall window
    if rise_fall_time_ini > 0.0:
        _n_win_ini = int(round(fs * rise_fall_time_ini))
        rise_fall_samples = np.arange(_n_win_ini)
        if type == 'linear':
            out_window[0:_n_win_ini] = np.arange(0, _n_win_ini) / _n_win_ini
        else:
            out_window[0:_n_win_ini] = np.sin(2.0 * np.pi * rise_fall_samples / (4.0 * (_n_win_ini - 1))) ** 2.0

    if rise_fall_time_end > 0.0:
        _n_win_end = int(round(fs * rise_fall_time_end))
        rise_fall_samples = np.arange(_n_win_end)
        if type == 'linear':
            out_window[-_n_win_end:] = np.arange(_n_win_end, 0., -1) / _n_win_end
        else:
            out_window[0:_n_win_end] = np.sin(2.0 * np.pi * rise_fall_samples / (4.0 *(_n_win_end - 1))) ** 2.0

    return np.atleast_2d(out_window).T


def rms(signal):
    return np.mean(signal ** 2.0, axis=0) ** 0.5


def lcm(num1, num2):
    return num1 * num2 / math.gcd(num1, num2)


def next_power_2(x: int ):
    value = 1
    while value <= x:
        value = value << 1
    return value